const foot = document.getElementById("footer");
const link = document.createElement(`a`);

link.textContent = "Learn More";
link.setAttribute("href", "#");

foot.append(link);


////////////////////////////////////////


const features = document.querySelector(".features")

const select = document.createElement("select")
select.id = "rating";

features.before(select);



function createOption(number) {
    const opt = document.createElement("option");
    opt.setAttribute("value", number);
    if (number > 1) {
        opt.textContent = `${number} Stars`
    } else {
        opt.textContent = `${number} Star`
    }
    return opt;
}

function addOpt(number) {
    const option = createOption(number);
    select.append(option);
}

for (let i = 1; i < 5; i++) {
    addOpt(i);
}